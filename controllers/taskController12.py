from models import models
import os
import json
import datetime
import pytz
import calendar     
import time    

def addTaskController(data_source,taskName,taskDate,status):
    try:
        now_utc = datetime.datetime.now(pytz.timezone('UTC'))
        now = now_utc.astimezone(pytz.timezone('Asia/Kolkata'))  
        curr_date = now.strftime('%Y-%m-%d')
        ts = calendar.timegm(time.gmtime())
        taskId="TASK"+str(ts)
        if(taskName=="" or taskDate=="" or status==""):
            return False
        else:
            data={
                "taskId":taskId,
                "taskName":taskName,
                "taskDate":taskDate,
                "createdDate":curr_date,
                "modifiedDate":curr_date,
                "status":status
                }
            addTask=models.addToJson(data_source,data)
            if(addTask):
                return True
            else:
                return False
    except (Exception) as error:
        print("------Error in controller",error)
        return False

def viewTaskController(data_source):
    try:
        data=models.viewJson(data_source)
        print("--------data",data)
        return data 
    except (Exception) as error:
        print("------Error in controller",error)
        return False