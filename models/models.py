import json

def addToJson(data_source,data):
    try:
        if(data):
            with open(data_source) as f:
                dataFile = json.load(f)
            task=data
            dataFile["data"].append(task)
            with open(data_source, 'w') as f:
                json.dump(dataFile, f)
            return True
        else:
            return False        
    except (Exception) as error:
        print("-------error",error)
        return False        

def viewJson(data_source):
    try:
        with open(data_source) as f:
            dataFile = json.load(f)
        return dataFile
    except (Exception) as error:
        print("-------error",error)
        return False        
