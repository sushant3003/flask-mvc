"""
Author      : Sushant Patil
Created At  : 09 August 2019
Description : This flask application is created for manipulating task list
Dependancies: Data file "data/taskData.json" which contain task list.
Assumptions : User can add any date's task either future or past

"""

from flask import Flask, request, jsonify,render_template
import json
import os
import re
from flask_cors import CORS
from controllers import taskController
from models import models

app = Flask(__name__)
CORS(app)
app.config["DEBUG"] = True

#This is the data file used for the application.
data_source = os.path.join(os.path.dirname(os.path.realpath(__file__)), "data/taskData.json")


#Name       :   addTask
#Description:   This function is used for adding task in task list
#Owner      :   Sushant Patil
#Created Date:  09-August-2019
#Modified Date: 11-August-2019
@app.route('/addTask',methods = ['GET'])
def addTask():
    try:
        taskName=request.args["taskName"]
        taskDate=request.args["taskDate"]
        status=request.args["status"]
        add=taskController.addTaskController(data_source,taskName,taskDate,status)
        if(add):
            result = { 
                "result":"Success",
                "status" : "200"
            }
        else:
            result = { 
                "result":"Fail",
                "status" : "200"
            }
        return jsonify(result)
    except (Exception) as error:
        result = { 
            "result":"Fail",
            "status" : "200"
        }
        return jsonify(result)


#Name       :   viewTask
#Description:   This function is used for viewing the task list
#Owner      :   Sushant Patil
#Created Date:  09-August-2019
#Modified Date: 11-August-2019
@app.route('/viewTask')
def viewTask():
    try:
        view=taskController.viewTaskController(data_source)
        result = { 
            "status" : "200"
        }
        return jsonify(result)
    except (Exception) as error:
        result = { 
            "result":"Fail",
            "status" : "200"
        }
        return jsonify(result)



#Name       :   renderviewTask
#Description:   Rendering the task list page
#Owner      :   Sushant Patil
#Created Date:  09-August-2019
#Modified Date: 11-August-2019       
@app.route('/')
def renderviewTask():
    try:
        viewData=taskController.viewTaskController(data_source)
        data=viewData['data']
        length=len(data)
        return render_template('viewTask.html',data=data,length=length)
    except (Exception) as error:
        return render_template('viewTask.html',data=[],length=0)


        
#Name       :   renderAddTask
#Description:   Rendering the add task list page
#Owner      :   Sushant Patil
#Created Date:  09-August-2019
#Modified Date: 11-August-2019 
@app.route('/AddTask')
def renderAddTask():
    try:
        return render_template('addTask.html')
    except (Exception) as error:
        return render_template('addTask.html')


if __name__ == "__main__":
    app.run(host="0.0.0.0", port=8888)