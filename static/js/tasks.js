/*
Author      : Sushant Patil
Name        : tasks.js
Created At  : 09 August 2019
Modified    : 11 August 2019  

*/
function saveTaskDetails(){
    console.log("----------------------------")
    api_url="/addTask"
    taskName=document.getElementById("taskName").value
    taskDate=document.getElementById("taskDate").value
    status=document.getElementById("select").value
    $("#errorTaskName").css({
        visibility: "hidden"
    });
    $("#errorTaskDate").css({
        visibility: "hidden"
    });
    if(taskName == "" || taskName ==undefined){
        $("#errorTaskName").css({
            visibility: "visible"
        });
    }
    else if(taskDate =="" || taskDate ==undefined){
        $("#errorTaskDate").css({
            visibility: "visible"
        });
    }else{
        data={
            taskDate:taskDate,
            taskName:taskName,
            status:status
        }
        $.ajax({
            url: api_url,
            data:data,
            contentType: "application/json",
            dataType: 'json',
            success: function(result){
                console.log("-------data",result.result)
                switch(result.status){
                    case "200":
                        if(result.result == "Success"){
                            alert("Task added")
                        }else{
                            alert("Something went wrong !")
                        }
                    break;
                    default:
                        alert("Something went wrong!")
                    break;
                }
            },
            error:function(error){
                alert("Something went wrong!")
            }
        });
    }
}
function navigateToAddTask(){
    window.location.href="/AddTask"
}
function backToHome(){
    window.location.href="/"
}